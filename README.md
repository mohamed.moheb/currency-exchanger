# Currency Exchanger App

To make the project start run the below commands:

- Change .env apiKey value with the apiKey value from Fixer.io

### `yarn install`
### `yarn start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in your browser.
