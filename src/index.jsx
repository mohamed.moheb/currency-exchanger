import React from 'react';
import ReactDOM from 'react-dom/client';
import {
  BrowserRouter,
  Routes,
  Route,
} from 'react-router-dom';
import HomePage from './Pages/HomePage';
import Details from './Pages/Details';
import './assets/css/index.css';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<HomePage />} />
        <Route path="/details/:currencyFrom" element={<Details />} />
        <Route path="/details/:currencyFrom/:currencyTo" element={<Details />} />
      </Routes>
    </BrowserRouter>
  </React.StrictMode>,
);
