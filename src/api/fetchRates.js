import superagent from 'superagent';

export default function fetchRates(base, symbols) {
  return superagent
    .get(`${process.env.REACT_APP_FIXER_ENDPOINT}/latest`)
    .set('apiKey', process.env.REACT_APP_FIXER_API_KEY)
    .query({ symbols, base })
    .then((res) => res.body.rates);
}
