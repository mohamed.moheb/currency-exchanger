import superagent from 'superagent';

export default function fetchConversionRate(fromCurrency, toCurrency, amount) {
  return superagent
    .get(`${process.env.REACT_APP_FIXER_ENDPOINT}/convert`)
    .set('apiKey', process.env.REACT_APP_FIXER_API_KEY)
    .query({ to: toCurrency, from: fromCurrency, amount })
    .then((res) => res.body.result);
}
