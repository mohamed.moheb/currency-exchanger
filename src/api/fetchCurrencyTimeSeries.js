import superagent from 'superagent';
import moment from "moment";

export default function fetchCurrencyTimeSeries(fromCurrency, toCurrency) {
    const startDate = moment().subtract(1, 'year').format('YYYY-MM-DD')
    const endDate = moment().subtract(1, 'day').format('YYYY-MM-DD')
    return superagent
        .get(`${process.env.REACT_APP_FIXER_ENDPOINT}/timeseries`)
        .set('apiKey', process.env.REACT_APP_FIXER_API_KEY)
        .query({ symbols: toCurrency, base: fromCurrency, start_date: startDate, end_date: endDate })
        .then((res) => formatData(res.body.rates));
}

function formatData (data) {
    return Object.keys(data).reduce((accum, d) => {
        accum.push([d,data[d][Object.keys(data[d])[0]]])
        return accum
    }, [])
}
