import Page from "../components/Page";
import RateConversionWidget from "../components/RateConversionWidget";
import {useEffect, useState} from "react";
import fetchRates from "../api/fetchRates";
import {Widget, WidgetsWrapper} from "../components/styled";

export default function HomePage() {
    const CURRENCIES = [
        'usd', 'egp', 'eur',
        'aed', 'aud', 'chf',
        'gbp', 'hkd', 'inr'
    ]
    const [currentRates, setCurrentRates] = useState([])
    const [fromCurrency, setFromCurrency] = useState('usd')
    const [toCurrency, setToCurrency] = useState('eur')
    const [amount, setAmount] = useState(1)
    useEffect(() => {
        getAllRates()
    }, [fromCurrency])

    const getAllRates = async () => {
        const rates = await fetchRates(fromCurrency, CURRENCIES.join(','))
        setCurrentRates(rates)
    }

    const handleOnChange = (type, data) => {
        if (type === 'from') {
            setFromCurrency(data)
            return
        }
        if( type === 'to') {
            setToCurrency(data)
            return
        }
        setAmount(data)
    }

    const renderWidgets = () => {
        return Object.keys(currentRates)?.map?.((currency) => {
            return <Widget key={currency}>{`${amount} ${fromCurrency.toUpperCase()} = ${(currentRates[currency] * amount).toFixed(3)} ${currency}`}</Widget>
        })
    }
  return (
    <>
      <Page>
          <RateConversionWidget
            currencies={CURRENCIES}
            fromCurrency={fromCurrency}
            toCurrency={toCurrency}
            onChange={handleOnChange}
          />
          <WidgetsWrapper>
            {renderWidgets()}
          </WidgetsWrapper>
      </Page>
    </>
  );
}
