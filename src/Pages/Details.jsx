import React, {useEffect, useState} from 'react';
import { useParams } from 'react-router-dom';
import Page from '../components/Page';
import RateConversionWidget from '../components/RateConversionWidget';
import EChart from '../components/EChart';
import fetchCurrencyTimeSeries from "../api/fetchCurrencyTimeSeries";

export default function Details() {
  const CURRENCIES = [
    'usd', 'egp', 'eur',
    'aed', 'aud', 'chf',
    'gbp', 'hkd', 'inr',
  ];
  const { currencyFrom, currencyTo } = useParams();

  const [toCurrency, setToCurrency] = useState(currencyTo || (currencyFrom === 'usd' ? 'eur' : 'usd'));
  const [timeSeriesData, setTimeSeriesData] = useState([])
    useEffect(() => {
        fetchData()
    }, [currencyFrom, currencyTo])
    const fetchData = async () => {
        const data = await fetchCurrencyTimeSeries(currencyFrom, toCurrency)
        setTimeSeriesData(data)
  }
  const handleOnChange = (type, data) => {
    if (type === 'to') {
      setToCurrency(data);
    }
  };
  return (
    <Page>
      <RateConversionWidget
        currencies={CURRENCIES}
        fromCurrency={currencyFrom}
        toCurrency={toCurrency}
        fromIsDisabled
        onChange={handleOnChange}
      />
      <div style={{ minHeight: '400px', width: '100%' }}>
        <EChart
          options={{
            xAxis: {
              type: 'time',
            },
              tooltip: {
                  trigger: 'axis'
              },
            yAxis: {
              type: 'value',
            },
            series: [
              {
                type: 'line',
                data: timeSeriesData
              },
            ],
          }}
        />
      </div>
    </Page>
  );
}
