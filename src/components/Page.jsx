import React from 'react';

import { PageWrapper } from './styled';
import Navigation from './Navigation';

export default function Page(props) {
  return (
    <PageWrapper>
      <Navigation currencies={['usd', 'eur']} />
      {props.children}
    </PageWrapper>
  );
}
