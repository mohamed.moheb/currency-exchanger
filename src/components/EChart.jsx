import React from 'react'
import * as echarts from 'echarts'
import 'zrender/lib/svg/svg'

export default function EChart(props) {
    const [echartRef, echartsInstance] = useECharts(props.options)

    React.useEffect(() => {
        echartsInstance?.setOption?.(props.options)
    }, [echartsInstance, props.options])

    React.useEffect(() => {
        if (props.showLoading) {
            echartsInstance?.showLoading?.('default', { text: '', color: '#000' })
        } else {
            echartsInstance?.hideLoading?.()
        }
    }, [echartsInstance, props.showLoading])

    React.useEffect(() => {
        if (echartsInstance) {
            props?.onEchartInit?.(echartsInstance, echartRef)
        }
    }, [echartsInstance, props?.onEchartInit])

    React.useEffect(() => {
        if (echartsInstance) {
            props?.onInViewport?.(echartsInstance, echartRef)
        }
    }, [props?.onInViewport])

    return (
        <div
            ref={echartRef}
            dir="ltr"
            style={{
                width: '100%',
                height: '100%',
            }}
        />
    )
}

EChart.echarts = echarts

export function useECharts(options) {
    const echartRef = React.useRef(null)

    const [echartInstance, setEchartInstance] = React.useState()

    React.useDebugValue({ 'echartInstance.options': echartInstance?.getOption?.() })
    React.useEffect(() => {
        if (echartRef.current) {
            const chart = echarts.init(
                echartRef.current,
                null,
                { renderer: 'canvas' }
            )
            chart.setOption(options)

            setEchartInstance(chart)
        }
    }, [echartRef, echartRef.current])

    return [echartRef, echartInstance]
}
