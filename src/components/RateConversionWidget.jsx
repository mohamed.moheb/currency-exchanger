import React, {useEffect, useState} from 'react'
import {
    AmountField,
    ConversionButton,
    ConversionResultWrapper,
    CurrencySelectorFields,
    RateConversionWrapper
} from "./styled";
import fetchConversionRate from "../api/fetchConversionRate";
import Spinner from './Spinner'

export default function RateConversionWidget(props) {
    const [amount, setAmount] = useState()
    const [conversionResult, setConversionResult] = useState()
    const [isLoading, setIsLoading] = useState(false)
    useEffect(() => {
        convert()
    }, [])
    const handleCurrencyChange = (direction, data) => {
        setConversionResult(undefined)
        if(direction === 'from') {
            props.onChange('from', data.target.value)
        } else {
            props.onChange('to', data.target.value)
        }
    }
    const flipCurrencies = () => {
        if(props?.fromIsDisabled) {
            return
        }
        const temp = props.toCurrency
        props.onChange('from', props.toCurrency)
        props.onChange('to', temp)
    }
    const validateAmount = () => {
        if(amount === '0') return true
        return !/^[+-]?([0-9]+\.?[0-9]*|\.[0-9]+)$/.exec(amount)
    }

    const handleAmountChange = (e) => {
        setAmount(e.target.value)
        setConversionResult(undefined)
    }

    const convert = async () => {
        setIsLoading(true)
        const conversionRate = await fetchConversionRate(props.fromCurrency, props.toCurrency, amount || 1)
        setIsLoading(false)
        setConversionResult(conversionRate)
        props.onChange('amount', amount || 1)
    }
    const renderOptionsMenu = () => {
        return props?.currencies.map((currency) => {
            return <option key={currency} value={currency}>{currency.toUpperCase()}</option>
        })
    }

    const renderConversionResult = () => {
        if(isLoading) {
            return <Spinner />
        }
        return (conversionResult && amount ) && (
            <ConversionResultWrapper>
                {`${amount} ${props.fromCurrency.toUpperCase()} = ${conversionResult} ${props.toCurrency.toUpperCase()}`}
                {!props?.fromIsDisabled && <a style={{ paddingLeft: '5px' }} href={`/details/${props.fromCurrency}/${props.toCurrency}`}>more details</a>}
            </ConversionResultWrapper>
        )
    }

    return (
        <RateConversionWrapper>
            <AmountField>
                <label>Amount:</label>
                <input type="text" placeholder="Amount" onKeyUp={handleAmountChange}/>
            </AmountField>
            <CurrencySelectorFields>
                <label>From:</label>
                <select
                    value={props.fromCurrency}
                    disabled={props?.fromIsDisabled || validateAmount()}
                    onChange={(e)=> {handleCurrencyChange('from', e)}}
                >
                    {renderOptionsMenu()}
                </select>
                <button style={{ margin: '4px' }} type="button" disabled={!props?.fromIsDisabled} onClick={flipCurrencies}>{'⇌'}</button>
                <label>To:</label>
                <select
                    value={props.toCurrency}
                    disabled={validateAmount()}
                    onChange={(e)=> {handleCurrencyChange('to', e)}}
                >
                    {renderOptionsMenu(props.toCurrency)}
                </select>
            </CurrencySelectorFields>
            <ConversionButton disabled={!amount} onClick={convert} type="button">Convert</ConversionButton>
            {renderConversionResult()}
        </RateConversionWrapper>
    );
}
