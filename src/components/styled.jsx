import styled from 'styled-components';

// Page Components
export const PageWrapper = styled.div`
    display: flex;
    flex-wrap: wrap;
    padding: 24px 25%;
    @media only screen and (max-width: 454px) {
        padding: 24px !important;
    }
`;
// Navigation Components
export const Logo = styled.img`
    width: 65px;
    float: left;
    padding-right: 5px;
`;
export const NavbarWrapper = styled.div`
    width: 100%;
    padding: 24px 0;
`;

export const CurrencyButtonDetails = styled.a`
    padding: 10px;
    border-radius: 4px;
    background-color: #862f3b;
    float: right;
    margin: 4px;
    text-decoration: none;
    color: #fff;
    @media only screen and (max-width: 454px) {
        float: left !important;
    }
`;

export const AppTitle = styled.div`
    float: left;
    padding-left: 4px;
    font-size: 24px;
    font-weight: 600;
    padding-top:14px;
`;

// Currency Conversion Component
export const AmountField = styled.div`
    padding: 24px 24px 4px 24px;
`;
export const RateConversionWrapper = styled.div`
    border: 1px solid #c7c7c7;
    border-radius: 4px;
`;
export const CurrencySelectorFields = styled.div`
    padding: 4px 24px 24px 24px;
`;
export const ConversionButton = styled.button`
    width: 90%;
    margin 0 0 16px 5%;
    padding: 4px;
    border-radius: 4px;
    background-color: #862f3b;
    color: #FFF;
    height: 40px;
    border: 1px solid #c7c7c7;
`;

export const ConversionResultWrapper = styled.div`
    padding: 18px 0;
    font-size: 14px;
    font-weight: 400;
    width: 100%;
    text-align: center;
`;

/* Widgets */

export const WidgetsWrapper = styled.div`
    display: flex;
    flex-wrap: wrap;
    width: 100%;
    padding-top 24px;
`;

export const Widget = styled.div`
    padding: 24px;
    margin: 5px;
    min-width: 200px;
    border: 1px solid #c7c7c7;
    border-radius: 4px;
`;
