import React from 'react'
import {AppTitle, CurrencyButtonDetails, Logo, NavbarWrapper} from "./styled";
import {useParams} from "react-router-dom";

export default function Navigation (props) {
    const { currencyFrom, currencyTo } = useParams()
    const BM_LOGO = 'https://png2.cleanpng.com/sh/996c6d92481809fd67d13203f115d602/L0KzQYm3VMI2N5ZoiZH0aYP2gLBuTfV5a5lmhtluLYLkhLa0hv9zbZpshp9ueHPrcbBuhb1uaaNwfeY2Y3BwgMb7hgIucZR0RadqZUC6crKAhcc4QZQ2RqYDNkm6SYK7UcUzPGc7SaMAOUm3SIK1kP5o/kisspng-exchange-rate-foreign-exchange-market-computer-ico-5ae07ba7e779c1.4869791415246611599481.png'
    const renderDetailsButtons = () => {
        return props?.currencies?.map((currency) => {
            return <CurrencyButtonDetails key={currency} href={`/details/${currency}`}>{`${currency.toUpperCase()} Details`}</CurrencyButtonDetails>
        })
    }
    return (
        <NavbarWrapper>
            <Logo src={BM_LOGO} />
            <AppTitle>Currency Exchanger</AppTitle>
            {renderDetailsButtons()}
            {(currencyTo || currencyFrom) && <CurrencyButtonDetails key="home" href={`/`}>Back to home</CurrencyButtonDetails>}
        </NavbarWrapper>
    )
}
